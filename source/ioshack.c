#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#include "ioshack.h"
#include "gecko.h"

void sync_before_read(void *p, u32 len)
{
	u32 a, b;

	a = (u32)p & ~0x1f;
	b = ((u32)p + len + 0x1f) & ~0x1f;

	for ( ; a < b; a += 32)
		asm("dcbi 0,%0" : : "b"(a));

	asm("sync ; isync");
}

void sync_after_write(const void *p, u32 len)
{
	u32 a, b;

	a = (u32)p & ~0x1f;
	b = ((u32)p + len + 0x1f) & ~0x1f;

	for ( ; a < b; a += 32)
		asm("dcbst 0,%0" : : "b"(a));

	asm("sync ; isync");
}

static inline u32 read32(u32 addr)
{
	u32 x;

	asm volatile("lwz %0,0(%1) ; sync" : "=r"(x) : "b"(0xc0000000 | addr));

	return x;
}

static inline u16 read16(u32 addr)
{
	u16 x;

	asm volatile("lhz %0,0(%1) ; sync" : "=r"(x) : "b"(0xc0000000 | addr));

	return x;
}

static inline u8 read8(u32 addr)
{
	u8 x;

	asm volatile("lbz %0,0(%1) ; sync" : "=r"(x) : "b"(0xc0000000 | addr));

	return x;
}

static inline void write32(u32 addr, u32 x)
{
	asm("stw %0,0(%1) ; eieio" : : "r"(x), "b"(0xc0000000 | addr));
}

static inline void write16(u32 addr, u16 x)
{
	asm("sth %0,0(%1) ; eieio" : : "r"(x), "b"(0xc0000000 | addr));
}

static inline void write8(u32 addr, u8 x)
{
	asm("stb %0,0(%1) ; eieio" : : "r"(x), "b"(0xc0000000 | addr));
}

int patch_isfs_permissions(u8* buf, u32 size)
{
	u32 i;
	int match_count = 0;
	u8 old_table[] = {0x42, 0x8b, 0xd0, 0x01, 0x25, 0x66, 0x42, 0x6d};
	u8 new_table[] = {0x42, 0x8b, 0xe0, 0x01, 0x25, 0x66, 0x42, 0x6d};

	for (i=0; i<size-sizeof old_table; i++) {
		if (!memcmp(buf + i, old_table, sizeof old_table)) {
			printf("Found isfs permissions @ 0x%x, patching.\n", (u32)(buf + i));
			memcpy(buf + i, new_table, sizeof new_table);
			i += sizeof new_table;
			match_count++;
			continue;
		}
	}
	return match_count;
}

int IOS_GetAhbProt()
{
	u32 ahb = read32(HW_AHBPROT);
	printf("IOS_GetAhbProt(): %08x\n", ahb);
	return 0;
}

int IOS_SetAhbProt()
{
	//printf("IOS_SetAhbProt();\n");
	write32(HW_AHBPROT, 0x00000001);
	if (read32(HW_AHBPROT) != 0x00000001)
		return 0;
	return 1;
}

int IOS_RemoveAhbProt()
{
	//printf("IOS_RemoveAhbProt();\n");
	write32(HW_AHBPROT, 0x00000000);
	if (read32(HW_AHBPROT) != 0x00000000)
		return 0;
	return 1;
}

int IOS_SetMemProt()
{
	//printf("IOS_SetMemProt();\n");
	write16(MEM_PROT, 0x0001);
	if (read16(MEM_PROT) != 0x0001)
		return 0;
	return 1;
}

int IOS_RemoveMemProt()
{
	//printf("IOS_RemoveMemProt();\n");
	write16(MEM_PROT, 0x0000);
	if (read16(MEM_PROT) != 0x0000)
		return 0;
	return 1;
}

int IOS_Patch()
{
	IOS_GetAhbProt();
	if (!IOS_RemoveMemProt())
		printf("Error with IOS_RemoveMemProt()\n");
	int ret = patch_isfs_permissions((u8*)0x93a11000, 0x00080000);
	printf("patch_isfs_permissions(): %d\n", ret);

	return 0;
}
