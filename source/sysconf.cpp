#include <gccore.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <vector>

#include "sysconf.h"
#include "gecko.h"

#define SYSCONF_UNK			0
#define SYSCONF_BIGARRAY	1
#define SYSCONF_SMLARRAY	2
#define SYSCONF_BYTE		3
#define SYSCONF_SHORT		4
#define SYSCONF_LONG		5
#define SYSCONF_LONGLONG	6
#define SYSCONF_BOOL		7

#define SYSCONF_LEN			0x4000

const char* s_types[] = {
	"UNK",
	"BIGARRAY",
	"SMALLARRAY",
	"BYTE",
	"SHORT",
	"LONG",
	"LONGLONG",
	"BOOL"
};

typedef struct
{
	int type;
	int length;
	void* data;
} single_entry;
std::vector<single_entry> entries;

typedef struct
{
	const char* name;
	int name_len;
	int type;
	int length;
} sysconf_entry_t;

const sysconf_entry_t s_entries[] = {
	{"IPL.CB",   6, SYSCONF_LONG    , 4},
	{"IPL.AR",   6, SYSCONF_BYTE    , 1},		// Aspect Ratio
	{"IPL.ARN",  7, SYSCONF_UNK     , 1},		// unknown size
	{"IPL.CD",   6, SYSCONF_BOOL    , 1},
	{"IPL.CD2",  7, SYSCONF_BOOL    , 1},
	{"IPL.DH",   6, SYSCONF_BYTE    , 1},
	{"IPL.E60",  7, SYSCONF_BYTE    , 1},
	{"IPL.EULA", 8, SYSCONF_BOOL    , 1},
	{"IPL.FRC",  7, SYSCONF_LONG    , 4},
	{"IPL.IDL",  7, SYSCONF_SMLARRAY, 0x2},		// LED modes; shutdown/idle; 0:1
	{"IPL.INC",  7, SYSCONF_LONG    , 4},
	{"IPL.LNG",  7, SYSCONF_BYTE    , 1},
	{"IPL.NIK",  7, SYSCONF_SMLARRAY, 0x16},	// OwnerNickName/0x16
	{"IPL.PC",   6, SYSCONF_SMLARRAY, 0x4A},	// Parental Control/0x4A
	{"IPL.PGS",  7, SYSCONF_BYTE    , 1},
	{"IPL.SSV",  7, SYSCONF_BYTE    , 1},
	{"IPL.SADR", 8, SYSCONF_BIGARRAY, 0x1008},	// SimpleAddressData|ID/0x1008
	{"IPL.SND",  7, SYSCONF_BYTE    , 1},
	{"IPL.UPT",  7, SYSCONF_BYTE    , 1},
	{"NET.CNF",  7, SYSCONF_UNK     , 1},		// unknown size
	{"NET.CTPC", 8, SYSCONF_LONG    , 4},
	{"NET.PROF", 8, SYSCONF_UNK     , 1},		// unknown size
	{"NET.WCPC", 8, SYSCONF_UNK     , 1},		// unknown size
	{"NET.WCFG", 8, SYSCONF_LONG    , 4},
	{"DEV.BTM",  7, SYSCONF_BYTE    , 1},
	{"DEV.VIM",  7, SYSCONF_UNK     , 1},		// unknown size
	{"DEV.CTC",  7, SYSCONF_UNK     , 1},		// unknown size
	{"DEV.DSM",  7, SYSCONF_BYTE    , 1},
	{"BT.DINF",  7, SYSCONF_BIGARRAY, 0x461},	// device info array: dinf_array:byte0x461
	{"BT.CDIF",  7, SYSCONF_BIGARRAY, 0x205},	// BtCmpDevInfoArray: byte0x205
	{"BT.SENS",  7, SYSCONF_LONG    , 4},
	{"BT.SPKV",  7, SYSCONF_BYTE    , 1},
	{"BT.MOT",   6, SYSCONF_BYTE    , 1},
	{"BT.BAR",   6, SYSCONF_BYTE    , 1},
	{"DVD.CNF",  7, SYSCONF_UNK     , 1},		// unknown size
	{"WWW.RST",  7, SYSCONF_BOOL    , 1},
	{"IPL.TID",  7, SYSCONF_LONGLONG, 8},
	{"MPLS.MOVIE", 10, SYSCONF_BOOL , 1},
};
int s_entries_len = 38;

#define SYSCONF_IPL_CB		0
#define SYSCONF_IPL_AR		1
#define SYSCONF_IPL_ARN		2
#define SYSCONF_IPL_CD		3
#define SYSCONF_IPL_CD2		4
#define SYSCONF_IPL_DH		5
#define SYSCONF_IPL_E60		6
#define SYSCONF_IPL_EULA	7
#define SYSCONF_IPL_FRC		8
#define SYSCONF_IPL_IDL		9
#define SYSCONF_IPL_INC		10
#define SYSCONF_IPL_LNG		11
#define SYSCONF_IPL_NIK		12
#define SYSCONF_IPL_PC		13
#define SYSCONF_IPL_PGS		14
#define SYSCONF_IPL_SSV		15
#define SYSCONF_IPL_SADR	16
#define SYSCONF_IPL_SND		17
#define SYSCONF_IPL_UPT		18
#define SYSCONF_NET_CNF		19
#define SYSCONF_NET_CTPC	20
#define SYSCONF_NET_PROF	21
#define SYSCONF_NET_WCPC	22
#define SYSCONF_NET_WCFG	23
#define SYSCONF_DEV_BTM		24
#define SYSCONF_DEV_VIM		25
#define SYSCONF_DEV_CTC		26
#define SYSCONF_DEV_DSM		27
#define SYSCONF_BT_DINF		28
#define SYSCONF_BT_CDIF		29
#define SYSCONF_BT_SENS		30
#define SYSCONF_BT_SPKV		31
#define SYSCONF_BT_MOT		32
#define SYSCONF_BT_BAR		33
#define SYSCONF_DVD_CNF		34
#define SYSCONF_WWW_RST		35
#define SYSCONF_IPL_TID		36
#define SYSCONF_MPLS_MOVIE	37

/* ipl::NandSDWorker::do_initialize_nand()
 * ---------------------------------------
 * BT.DINF : get()
 * IPL.CD : 0
 * IPL.CD2 : 0
 */

/* ipl::System::init() : if IPL.CD and IPL.CD2 == 0
 * -------------------
 * IPL.NIK : memset()
 * IPL.AR : 0
 * BT.BAR : 0
 * BT.SENS : 3
 * IPL.SSV : 0
 * SC.LNG : based_on_setting.txt
 * BT.SPKV : 0x58
 * IPL.PC : memset()
 * NET.CTPC : 0
 * WWW.RST : 0
 */

/* returns true on success */
bool CreateItem(int id, void* buffer, short len)
{
	if ((id >= s_entries_len) || (id < 0))
		return false;
	if (len != s_entries[id].length)
		return false;

	single_entry entry;
	entry.type = id;
	entry.length = s_entries[id].length;
	void* new_buffer = malloc(len * sizeof(char));
	memcpy(new_buffer, buffer, len);
	entry.data = new_buffer;
	entries.push_back(entry);

	return true;
}

/* returns true on success */
bool DeleteItem(int id)
{
	u32 ii;
	for (ii = 0; ii < entries.size(); ii++)
	{
		if (entries[ii].type == id)
		{
			if (entries[ii].data)
				free(entries[ii].data);
			entries.erase(entries.begin()+ii);
			return true;
		}
	}
	return false;
}

/* returns true on success */
bool ReplaceItem(int id, void* buffer, short len)
{
	if (id >= s_entries_len)
		return false;
	u32 ii;
	for (ii = 0; ii < entries.size(); ii++)
	{
		if (entries[ii].type == id)
			break;
	}
	if (ii < entries.size())
	{
		DeleteItem(id);
	}
	CreateItem(id, buffer, len);

	return true;
}

u8 GetU8Item(int type)
{
	for (u32 ii = 0; ii < entries.size(); ii++)
	{
		if (entries[ii].type == type)
			return *(u8*)(entries[ii].data);
	}
	return 0xff;
}
s8 GetS8Item(int type)
{
	for (u32 ii = 0; ii < entries.size(); ii++)
	{
		if (entries[ii].type == type)
			return *(s8*)(entries[ii].data);
	}
	return -127;
}
u16 GetU16Item(int type)
{
	for (u32 ii = 0; ii < entries.size(); ii++)
	{
		if (entries[ii].type == type)
			return *(u16*)(entries[ii].data);
	}
	return 0xffff;
}
u32 GetU32Item(int type)
{
	for (u32 ii = 0; ii < entries.size(); ii++)
	{
		if (entries[ii].type == type)
			return *(u32*)(entries[ii].data);
	}
	return 0xffffffff;
}
u64 GetU64Item(int type)
{
	for (u32 ii = 0; ii < entries.size(); ii++)
	{
		if (entries[ii].type == type)
			return *(u64*)(entries[ii].data);
	}
	return 0xffffffffffffffffULL;
}
bool GetBoolItem(int type)
{
	for (u32 ii = 0; ii < entries.size(); ii++)
	{
		if (entries[ii].type == type)
		{
			if (*(u8*)(entries[ii].data))
				return true;
			else
				return false;
		}
	}
	return false;
}
bool ItemExists(int type)
{
	for (u32 ii = 0; ii < entries.size(); ii++)
	{
		if (entries[ii].type == type)
			return true;
	}
	return false;
}
void* GetArrayItem(int type)
{
	for (u32 ii = 0; ii < entries.size(); ii++)
	{
		if (entries[ii].type == type)
			return entries[ii].data;
	}
	return NULL;
}

bool SYSCONF_SetAspectRatio(u8 value)
{
	u8 temp = (value <= SYSCONF_AR_16_9) ? temp : SYSCONF_AR_4_3;
	return ReplaceItem(SYSCONF_IPL_AR, (void*)&temp, 1);
}
u8 SYSCONF_GetAspectRatio()
{
	u8 value = GetU8Item(SYSCONF_IPL_AR);
	return (value <= SYSCONF_AR_16_9) ? value : SYSCONF_AR_4_3;
}
bool SYSCONF_SetDisplayOffsetH(s8 value)
{
	s8 temp = value;;
	if (value < -0x20)
		temp = -0x20;
	if (value > 0x20)
		temp = 0x20;
	return ReplaceItem(SYSCONF_IPL_DH, (void*)&temp, 1);
}
s8 SYSCONF_GetDisplayOffsetH()
{
	s8 value = GetS8Item(SYSCONF_IPL_DH);
	return (value != -127) ? value : 0;
}
bool SYSCONF_SetEuRgb60Mode(u8 value)
{
	u8 temp = (value == 1) ? value : 0;
	return ReplaceItem(SYSCONF_IPL_E60, (void*)&temp, 1);
}
u8 SYSCONF_GetEuRgb60Mode()
{
	u8 value = GetU8Item(SYSCONF_IPL_E60);
	return (value <= 1) ? value : 0;
}
bool SYSCONF_SetIdleMode(bool shutdown, bool idle)
{
	u16 temp = idle ? 1 : 0;
	if (shutdown)
		temp |= (1 << 8);
	return ReplaceItem(SYSCONF_IPL_IDL, (void*)&temp, 2);
}
void SYSCONF_GetIdleMode(bool* shutdown, bool *idle)
{
	u16 value = GetU16Item(SYSCONF_IPL_IDL);
	*shutdown = (value & 0x100) ? true : false;
	*idle = (value & 0x1) ? true : false;
}
/* XXX - error checking for language */
bool SYSCONF_SetLanguage(u8 value)
{
	u8 temp = value;
	return ReplaceItem(SYSCONF_IPL_LNG, (void*)&temp, 1);
}
u8 SYSCONF_GetLanguage()
{
	return GetU8Item(SYSCONF_IPL_LNG);
}
bool SYSCONF_SetParentalControl(void* buffer)
{
	return ReplaceItem(SYSCONF_IPL_PC, buffer, 0x4A);
}
bool SYSCONF_GetParentalControl(void** buffer)
{
	if ((!buffer) || (!*buffer))
		return false;
	void* temp = GetArrayItem(SYSCONF_IPL_PC);
	if (temp)
	{
		memcpy(*buffer, temp, 0x4A);
		return true;
	}
	return false;
}
bool SYSCONF_SetProgressiveMode(u8 value)
{
	u8 temp = (value <= 1) ? value : 0;
	return ReplaceItem(SYSCONF_IPL_PGS, (void*)&temp, 1);
}
u8 SYSCONF_GetProgressiveMode()
{
	u8 value = GetU8Item(SYSCONF_IPL_PGS);
	return (value <= 1) ? value : 0;
}
bool SYSCONF_SetScreenSaverMode(u8 value)
{
	u8 temp = (value <= 1) ? value : 0;
	return ReplaceItem(SYSCONF_IPL_SSV, (void*)&temp, 1);
}
u8 SYSCONF_GetScreenSaverMode()
{
	u8 value = GetU8Item(SYSCONF_IPL_SSV);
	return (value <= 1) ? value : 0;
}
bool SYSCONF_SetSoundMode(u8 value)
{
	u8 temp = (value <= SYSCONF_SND_SURROUND) ? value : 1;
	return ReplaceItem(SYSCONF_IPL_SND, (void*)&temp, 1);
}
u8 SYSCONF_GetSoundMode()
{
	u8 value = GetU8Item(SYSCONF_IPL_SND);
	return (value <= SYSCONF_SND_SURROUND) ? value : SYSCONF_SND_STEREO;
}
bool SYSCONF_SetCounterBias(u32 value)
{
	u32 temp = value;
	return ReplaceItem(SYSCONF_IPL_CB, (void*)&temp, 4);
}
u32 SYSCONF_GetCounterBias()
{
	u32 value = GetU32Item(SYSCONF_IPL_CB);
	return (value != 0xffffffff) ? value : 0xB49D800;
}
/* XXX - BtDeviceArrayInfo error checking */
bool SYSCONF_SetBtDeviceArrayInfo(void* buffer)
{
	return ReplaceItem(SYSCONF_BT_DINF, buffer, 0x461);
}
bool SYSCONF_GetBtDeviceArrayInfo(void** buffer)
{
	if ((!buffer) || (!*buffer))
		return false;
	void* temp = GetArrayItem(SYSCONF_BT_DINF);
	if (temp)
	{
		memcpy(*buffer, temp, 0x461);
		return true;
	}
	return false;
}
/* XXX - BtCmpDevInfoArray error checking */
bool SYSCONF_SetBtCmpDevInfoArray(void* buffer)
{
	return ReplaceItem(SYSCONF_BT_CDIF, buffer, 0x205);
}
bool SYSCONF_GetBtCmpDevInfoArray(void** buffer)
{
	if ((!buffer) || (!*buffer))
		return false;
	void* temp = GetArrayItem(SYSCONF_BT_CDIF);
	if (temp)
	{
		memcpy(*buffer, temp, 0x205);
		return true;
	}
	return false;
}
bool SYSCONF_SetBtDpdSensibility(u32 value)
{
	u32 temp = value;
	if (value < 1)
		temp = 1;
	if (value > 5)
		temp = 5;
	return ReplaceItem(SYSCONF_BT_SENS, (void*)&temp, 4);
}
u32 SYSCONF_GetBtDpdSensibility()
{
	u32 value = GetU32Item(SYSCONF_BT_SENS);
	if (value == 0xffffffff)
		return 2;
	if (value < 1)
		return 1;
	if (value > 5)
		return 5;
	return value;
}
bool SYSCONF_SetWwwRestrictions(bool value)
{
	u8 temp = value ? 1 : 0;
	return ReplaceItem(SYSCONF_WWW_RST, (void*)&temp, 1);
}
bool SYSCONF_GetWwwRestrictions()
{
	return GetBoolItem(SYSCONF_WWW_RST);
}
bool SYSCONF_SetWpadMotorMode(u8 value)
{
	u8 temp = (value <= 1) ? value : 1;
	return ReplaceItem(SYSCONF_BT_MOT, (void*)&temp, 1);
}
u8 SYSCONF_GetWpadMotorMode()
{
	u8 value = GetU8Item(SYSCONF_BT_MOT);
	return (value <= 1) ? value : 1;
}
bool SYSCONF_SetWpadSensorBarPosition(u8 value)
{
	u8 temp = (value <= SYSCONF_BAR_BOTTOM) ? value : 0;
	return ReplaceItem(SYSCONF_BT_BAR, (void*)&temp, 1);
}
u8 SYSCONF_GetWpadSensorBarPosition()
{
	u8 value = GetU8Item(SYSCONF_BT_BAR);
	return (value <= SYSCONF_BAR_BOTTOM) ? value : SYSCONF_BAR_TOP;
}
bool SYSCONF_SetWpadSpeakerVolume(u8 value)
{
	u8 temp = (value <= 127) ? value : 89;
	return ReplaceItem(SYSCONF_BT_SPKV, (void*)&temp, 1);
}
u8 SYSCONF_GetWpadSpeakerVolume()
{
	u8 value = GetU8Item(SYSCONF_BT_SPKV);
	if (value == 0xff)
		return 89;
	if (value > 127)
		return 127;
	return value;
}
/* XXX - nickname error checking */
bool SYSCONF_SetOwnerNickName(void* buffer)
{
	/* 0x14bytes of utf_be16 names. u16 of name_len */
	return ReplaceItem(SYSCONF_IPL_NIK, buffer, 0x16);
}
bool SYSCONF_GetOwnerNickName(void** buffer)
{
	if ((!buffer) || (!*buffer))
		return false;
	void* temp = GetArrayItem(SYSCONF_IPL_NIK);
	if (temp)
	{
		memcpy(*buffer, temp, 0x16);
		return true;
	}
	return false;
}
/* XXX - simple address data error checking */
bool SYSCONF_SetSimpleAddressData(void* buffer)
{
	return ReplaceItem(SYSCONF_IPL_SADR, buffer, 0x1008);
}
/* u8 SYSCONF_GetSimpleAddressID() */
bool SYSCONF_GetSimpleAddressData(void** buffer)
{
	if ((!buffer) || (!*buffer))
		return false;
	void* temp = GetArrayItem(SYSCONF_IPL_SADR);
	if (temp)
	{
		memcpy(*buffer, temp, 0x1008);
		return true;
	}
	return false;
}
bool SYSCONF_SetNetContentRestrictions(u32 value)
{
	u32 temp = value;
	return ReplaceItem(SYSCONF_NET_CTPC, (void*)&temp, 4);
}
u32 SYSCONF_GetNetContentRestrictions()
{
	u32 value = GetU32Item(SYSCONF_NET_CTPC);
	return (value == 0xffffffff) ? 0 : value;
}
bool SYSCONF_SetConfigDoneFlag(bool value)
{
	u8 temp = value ? 1 : 0;
	return ReplaceItem(SYSCONF_IPL_CD, (void*)&temp, 1);
}
bool SYSCONF_GetConfigDoneFlag()
{
	return GetBoolItem(SYSCONF_IPL_CD);
}
bool SYSCONF_SetConfigDoneFlag2(bool value)
{
	u8 temp = value ? 1 : 0;
	return ReplaceItem(SYSCONF_IPL_CD2, (void*)&temp, 1);
}
bool SYSCONF_GetConfigDoneFlag2()
{
	return GetBoolItem(SYSCONF_IPL_CD2);
}
bool SYSCONF_SetUpdateType(u8 value)
{
	u8 temp = (value <= 2) ? value : 0;
	return ReplaceItem(SYSCONF_IPL_UPT, (void*)&temp, 1);
}
u8 SYSCONF_GetUpdateType()
{
	u8 value = GetU8Item(SYSCONF_IPL_UPT);
	return (value <= 2) ? value : 0;
}
bool SYSCONF_SetEula(bool value)
{
	u8 temp = value ? 1 : 0;
	return ReplaceItem(SYSCONF_IPL_EULA, (void*)&temp, 1);
}
bool SYSCONF_GetEula()
{
	return GetBoolItem(SYSCONF_IPL_EULA);
}
/* XXX - wc flags error checking */
bool SYSCONF_SetWCFlags(u32 value)
{
	u32 temp = value;
	return ReplaceItem(SYSCONF_NET_WCFG, (void*)&temp, 4);
}
u32 SYSCONF_GetWCFlags()
{
	return GetU32Item(SYSCONF_NET_WCFG);
}
bool SYSCONF_SetFreeChannelAppCount(u32 value)
{
	u32 temp = value;
	return ReplaceItem(SYSCONF_IPL_FRC, (void*)&temp, 4);
}
u32 SYSCONF_GetFreeChannelAppCount()
{
	return GetU32Item(SYSCONF_IPL_FRC);
}
bool SYSCONF_SetInstalledChannelAppCount(u32 value)
{
	u32 temp = value;
	return ReplaceItem(SYSCONF_IPL_INC, (void*)&temp, 4);
}
u32 SYSCONF_GetInstalledChannelAppCount()
{
	return GetU32Item(SYSCONF_IPL_INC);
}
bool SYSCONF_SetTid(u64 value)
{
	u64 temp = value;
	return ReplaceItem(SYSCONF_IPL_TID, (void*)&temp, 8);
}
u64 SYSCONF_GetTid()
{
	return GetU64Item(SYSCONF_IPL_TID);
}
bool SYSCONF_SetMotionPlusMoviePlayed(bool value)
{
	u8 temp = value ? 1 : 0;
	return ReplaceItem(SYSCONF_MPLS_MOVIE, (void*)&temp, 1);
}
bool SYSCONF_GetMotionPlusMoviePlayed()
{
	return GetBoolItem(SYSCONF_MPLS_MOVIE);
}

void ReadSysconf(void* buffer)
{
	/* clear entries vector */
	entries.clear();

	u32 buff  = (u32)buffer;
	u32 magic = *(u32*)(buff + 0);
	u32 magic2 = *(u32*)(buff + SYSCONF_LEN - 4);
	u16 n_ent = *(u16*)(buff + 4);
	if ((magic != 'SCv0') || (magic2 != 'SCed'))
	{
		printf("magic != 'SCv0\n");
	}
	else
	{
		u32 ii;
		int jj;
		for (ii = 0; ii < n_ent; ii++)
		{
			bool found = false;
			u16 off = *(u16*)(buff + 6 + ii*2);
			u8 val  = *(u8*)(buff + off);
			u8 name_len = (val & 0x1F) + 1;
			u8 type     = (val >> 5) & 0x7;
			for (jj = 0; jj < s_entries_len; jj++)
			{
				if (name_len != s_entries[jj].name_len)
					continue;
				if (!memcmp((void*)(buff+off+1), s_entries[jj].name, s_entries[jj].name_len))
				{
					//printf("Found %s @ %d: %d %d %#x\n",
					//		s_entries[jj].name, ii, s_entries[jj].name_len, s_entries[jj].type, s_entries[jj].length);
					if (type == SYSCONF_UNK)
					{
						printf("Found %s @ %d: %d %d %#x\n",
							s_entries[jj].name, ii, s_entries[jj].name_len, s_entries[jj].type, s_entries[jj].length);
						printf("\tSYSCONF_UNK:\n");
						ghexdump((void*)(buff+off), 0x10);
					}
					if (type != s_entries[jj].type)
					{
						printf("Found %s @ %d: %d %d %#x\n",
							s_entries[jj].name, ii, s_entries[jj].name_len, s_entries[jj].type, s_entries[jj].length);
						printf("\tBad type: expected %d got %d\n", type, s_entries[jj].type);
					}
					
					u32 l = 0;
					u32 w = buff + off + 1 + name_len;
					void* buf = NULL;
					single_entry entry;
					entry.type = jj;
					entry.length = s_entries[jj].length;
					switch (type)
					{
						case SYSCONF_UNK:
							buf = static_cast<void*>(malloc(sizeof(int)));
							memcpy(buf, (void*)w, sizeof(int));
							break;
						case SYSCONF_BIGARRAY:
							l = *(u16*)w;
							buf = static_cast<void*>(malloc(l+1 * sizeof(char)));
							memcpy(buf, (void*)(w+sizeof(short)), l+1);
							break;
						case SYSCONF_SMLARRAY:
							l = *(u8*)w;
							buf = static_cast<void*>(malloc(l+1 * sizeof(char)));
							memcpy(buf, (void*)(w+sizeof(char)), l+1);
							break;
						case SYSCONF_BYTE:
							buf = static_cast<void*>(malloc(sizeof(char)));
							memcpy(buf, (void*)w, sizeof(char));
							break;
						case SYSCONF_SHORT:
							buf = static_cast<void*>(malloc(sizeof(short)));
							memcpy(buf, (void*)w, sizeof(short));
							break;
						case SYSCONF_LONG:
							buf = static_cast<void*>(malloc(sizeof(int)));
							memcpy(buf, (void*)w, sizeof(int));
							break;
						case SYSCONF_LONGLONG:
							buf = static_cast<void*>(malloc(sizeof(u64)));
							memcpy(buf, (void*)w, sizeof(u64));
							break;
						case SYSCONF_BOOL:
							buf = static_cast<void*>(malloc(sizeof(char)));
							memcpy(buf, (void*)w, sizeof(char));
							break;
						default:
							printf("Default\n");
							break;
					}
					entry.data = buf;
					entries.push_back(entry);
					found = true;
				}
			}
			if (!found)
				printf("Entry not found in table: %d [%.10s]\n", ii, (char*)(buff+off+1));
		}

#if 1
		for (ii = 0; ii < entries.size(); ii++)
		{
			if (entries[ii].data)
			{
				printf("\nType: %s [%s]\n", s_entries[entries[ii].type].name,
						s_types[s_entries[entries[ii].type].type]);
				ghexdump(entries[ii].data, entries[ii].length);
				//free(entries[ii].data);
			}
		}
		//entries.clear();
#endif
	}
}

void* WriteSysconf()
{
	void* buffer = malloc(SYSCONF_LEN * sizeof(char));
	if (!buffer)
		return buffer;
	memset(buffer, 0, SYSCONF_LEN);
	u32 buff = (u32)buffer;
	*(u32*)(buff + 0) = 'SCv0';
	*(u16*)(buff + 4) = (u16)entries.size();
	std::vector<u16> offsets;

	u32 ii = 0;
	u16 offset = 4 + 2 + (entries.size()+1)*2;
	u16 p = 4 + 2;
	for (ii = 0; ii < entries.size(); ii++)
	{
		*(u16*)(buff + (SYSCONF_LEN-4) - 2 - (entries[ii].type * 2)) = p; p += 2;
		offsets.push_back(offset);

		offset += 1 + s_entries[entries[ii].type].name_len + entries[ii].length;
		int type = s_entries[entries[ii].type].type;
		if (type == SYSCONF_BIGARRAY)
			offset += 2;
		else if (type == SYSCONF_SMLARRAY)
			offset += 1;
	}
	offsets.push_back(offset);
	for (ii = 0; ii < offsets.size(); ii++)
	{
		u16 off = 6 + ii*2;
		*(u16*)(buff + off) = offsets[ii];
	}
	for (ii = 0; ii < entries.size(); ii++)
	{
		u8 t = s_entries[entries[ii].type].type & 0x7;
		u8 n = (s_entries[entries[ii].type].name_len - 1) & 0x1f;
		u8 val = (t<<5) | n;
		u16 off = offsets[ii];
		*(u8*)(buff + off) = val;
		off++;
		memcpy((void*)(buff+off), s_entries[entries[ii].type].name, s_entries[entries[ii].type].name_len);
		off += s_entries[entries[ii].type].name_len;
		int type = s_entries[entries[ii].type].type;
		if (type == SYSCONF_BIGARRAY)
		{
			*(u16*)(buff + off) = entries[ii].length-1;
			off += 2;
		}
		else if (type == SYSCONF_SMLARRAY)
		{
			*(u8*)(buff + off) = (u8)(entries[ii].length-1);
			off++;
		}

		memcpy((void*)(buff+off), entries[ii].data, entries[ii].length);
	}

	*(u32*)(buff + (SYSCONF_LEN - 4)) = 'SCed';
	return buffer;
}

int CheckSysconf(void* buffer)
{
	int ret = 0;

	u32 buff  = (u32)buffer;
	u32 magic = *(u32*)(buff + 0);
	u32 magic2 = *(u32*)(buff + (SYSCONF_LEN - 4));
	u16 n_ent = *(u16*)(buff + 4);
	if ((magic != 'SCv0') || (magic2 != 'SCed'))
	{
		printf("magic bad, rarrgghghghgh\n");
		ret = -1;
	}
	else
	{
		int ii, jj;
		for (ii = 0; ii < n_ent; ii++)
		{
			bool found = false;
			u16 off = *(u16*)(buff + 6 + ii*2);
			u8 val  = *(u8*)(buff + off);
			u8 name_len = (val & 0x1F) + 1;
			u8 type     = (val >> 5) & 0x7;
			for (jj = 0; jj < s_entries_len; jj++)
			{
				if (name_len != s_entries[jj].name_len)
					continue;
				if (!memcmp((void*)(buff+off+1), s_entries[jj].name, s_entries[jj].name_len))
				{
					printf("Found %s @ %d: %d %d %#x\n",
							s_entries[jj].name, ii, s_entries[jj].name_len, s_entries[jj].type, s_entries[jj].length);
					if (type == SYSCONF_BIGARRAY)
					{
						u16 len = *(u16*)(buff+off+1+name_len);
						if (s_entries[jj].length != len+1)
							printf("Bad big array length [%#x:%#x]\n", len+1, s_entries[jj].length);
					}
					if (type == SYSCONF_SMLARRAY)
					{
						u8 len = *(u8*)(buff+off+1+name_len);
						if (s_entries[jj].length != len+1)
							printf("Bad small array length [%#x:%#x]\n", len+1, s_entries[jj].length);
					}
					if (type == SYSCONF_UNK)
					{
						printf("\tSYSCONF_UNK:\n");
						ghexdump((void*)(buff+off), 0x10);
						ret = -2;
					}
					if (type != s_entries[jj].type)
					{
						printf("\tBad type: expected %d got %d\n", type, s_entries[jj].type);
						ret = -4;
					}
					found = true;
				}
			}
			if (!found) {
				printf("Entry not found in table: %d [%.10s]\n", ii, (char*)(buff+off+1));
				//ret = -6;
			}
		}
	}

	return ret;
}

void DebugSysconf(void* buffer)
{
	u32 buff = (u32)buffer;
	u32* buf = (u32*)buffer;
	u16* buf16 = (u16*)buffer;
	u32 magic = buf[0];
	u32 magic2 = *(u32*)(buff + (SYSCONF_LEN - 4));
	u16 num_entries = buf16[2];
	if ((magic != 'SCv0') || (magic2 != 'SCed'))
	{
		printf("magic bad\n");
	}
	else
	{
		printf("Num Entries: %d\n", num_entries);
		int ii;
		for (ii = 0; ii < num_entries; ii++)
		{
			u16 off = buf16[3+ii];
			printf("Offset: %04x\n", off);
			u8 val = *(u8*)(buff + off);
			u8 name_len = (val & 0x1F) + 1;
			u8 type     = (val >> 5) & 0x7;
			char name[0x40];
			snprintf(name, name_len+1, "%s", (char*)(buff + off + 1));
			printf("%02x: %s\n", val, name);
			u32 l = 0;
			u64 d = 0;
			u32 w = buff + off + 1 + name_len;
			switch (type)
			{
				case SYSCONF_UNK:
					l = *(u32*)w;
					printf("UNK: %d\n", l);
					break;
				case SYSCONF_BIGARRAY:
					l = *(u16*)w;
					printf("BIGARRAY: %#x+1 len\n", l);
					ghexdump((void*)(w+sizeof(short)), l+1);
					break;
				case SYSCONF_SMLARRAY:
					l = *(u8*)w;
					printf("SMALLARRAY: %#x+1 len\n", l);
					ghexdump((void*)(w+sizeof(char)), l+1);
					break;
				case SYSCONF_BYTE:
					l = *(u8*)w;
					printf("BYTE: %d\n", l);
					break;
				case SYSCONF_SHORT:
					l = *(u16*)w;
					printf("SHORT: %d\n", l);
					break;
				case SYSCONF_LONG:
					l = *(u32*)w;
					printf("LONG: %d\n", l);
					break;
				case SYSCONF_LONGLONG:
					d = *(u64*)w;
					printf("LONGLONG: %016llx\n", d);
					break;
				case SYSCONF_BOOL:
					l = *(u8*)w;
					printf("BOOL: %d\n", l);
					break;
				default:
					printf("Default\n");
					break;
			}
		}
	}
}

