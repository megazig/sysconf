#ifndef __SYSCONF_H
#define __SYSCONF_H

#define SYSCONF_AR_4_3	0
#define SYSCONF_AR_16_9	1
/* setters and getters */
bool SYSCONF_SetAspectRatio(u8 value);
u8 SYSCONF_GetAspectRatio(void);
bool SYSCONF_SetDisplayOffsetH(s8 value);
s8 SYSCONF_GetDisplayOffsetH(void);
bool SYSCONF_SetEuRgb60Mode(u8 value);
u8 SYSCONF_GetEuRgb60Mode(void);
bool SYSCONF_SetIdleMode(bool shutdown, bool idle);
void SYSCONF_GetIdleMode(bool* shutdown, bool *idle);
bool SYSCONF_SetLanguage(u8 value);
u8 SYSCONF_GetLanguage(void);
bool SYSCONF_SetParentalControl(void* buffer);
bool SYSCONF_GetParentalControl(void** buffer);
bool SYSCONF_SetProgressiveMode(u8 value);
u8 SYSCONF_GetProgressiveMode(void);
bool SYSCONF_SetScreenSaverMode(u8 value);
u8 SYSCONF_GetScreenSaverMode(void);
#define SYSCONF_SND_MONO		0
#define SYSCONF_SND_STEREO		1
#define SYSCONF_SND_SURROUND	2
bool SYSCONF_SetSoundMode(u8 value);
u8 SYSCONF_GetSoundMode(void);
bool SYSCONF_SetCounterBias(u32 value);
u32 SYSCONF_GetCounterBias(void);
bool SYSCONF_SetBtDeviceArrayInfo(void* buffer);
bool SYSCONF_GetBtDeviceArrayInfo(void** buffer);
bool SYSCONF_SetBtCmpDevInfoArray(void* buffer);
bool SYSCONF_GetBtCmpDevInfoArray(void** buffer);
bool SYSCONF_SetBtDpdSensibility(u32 value);
u32 SYSCONF_GetBtDpdSensibility(void);
bool SYSCONF_SetWwwRestrictions(bool value);
bool SYSCONF_GetWwwRestrictions(void);
bool SYSCONF_SetWpadMotorMode(u8 value);
u8 SYSCONF_GetWpadMotorMode(void);
#define SYSCONF_BAR_BOTTOM	0
#define SYSCONF_BAR_TOP		1
bool SYSCONF_SetWpadSensorBarPosition(u8 value);
u8 SYSCONF_GetWpadSensorBarPosition(void);
bool SYSCONF_SetWpadSpeakerVolume(u8 value);
u8 SYSCONF_GetWpadSpeakerVolume(void);
bool SYSCONF_SetOwnerNickName(void* buffer);
bool SYSCONF_GetOwnerNickName(void** buffer);
bool SYSCONF_SetSimpleAddressData(void* buffer);
bool SYSCONF_GetSimpleAddressData(void** buffer);
bool SYSCONF_SetNetContentRestrictions(u32 value);
u32 SYSCONF_GetNetContentRestrictions(void);
bool SYSCONF_SetConfigDoneFlag(bool value);
bool SYSCONF_GetConfigDoneFlag(void);
bool SYSCONF_SetConfigDoneFlag2(bool value);
bool SYSCONF_GetConfigDoneFlag2(void);
bool SYSCONF_SetUpdateType(u8 value);
u8 SYSCONF_GetUpdateType(void);
bool SYSCONF_SetEula(bool value);
bool SYSCONF_GetEula(void);
bool SYSCONF_SetWCFlags(u32 value);
u32 SYSCONF_GetWCFlags(void);
bool SYSCONF_SetFreeChannelAppCount(u32 value);
u32 SYSCONF_GetFreeChannelAppCount(void);
bool SYSCONF_SetInstalledChannelAppCount(u32 value);
u32 SYSCONF_GetInstalledChannelAppCount(void);
bool SYSCONF_SetTid(u64 value);
u64 SYSCONF_GetTid(void);
bool SYSCONF_SetMotionPlusMoviePlayed(bool value);
bool SYSCONF_GetMotionPlusMoviePlayed(void);

void ReadSysconf(void* buffer);
void* WriteSysconf();
int CheckSysconf(void* buffer);
void DebugSysconf(void* buffer);

#endif
