#include <stdio.h>
#include <stdlib.h>
#include <gccore.h>
#include <unistd.h>
#include <wiiuse/wpad.h>
#include <string.h>

#include <debug.h>

#include "ioshack.h"
#include "gecko.h"
#include "sysconf.h"

static void *xfb = NULL;
static GXRModeObj *rmode = NULL;

static inline u32 read32(u32 addr)
{
	u32 x;

	asm volatile("lwz %0,0(%1) ; sync" : "=r"(x) : "b"(0xc0000000 | addr));

	return x;
}

#include <malloc.h>
/* *buffer needs to be freed outside of ReadNandFile() */
int ReadNandFile(const char* path, void** buffer, int* len)
{
	int ret = 0;
	int readFp = ISFS_Open(path, ISFS_OPEN_RW);
	if (readFp < 0)
	{
		printf("Failed to open file: %s\n", path);
		ret = -1;
	}
	else
	{
		printf("ISFS_Open() returned %d\n", readFp);
		int seek = ISFS_Seek(readFp, 0, SEEK_SET);
		printf("ISFS_Seek() returned %d\n", seek);
		fstats* read_status = static_cast<fstats*>(memalign(32, 8*sizeof(char)));
		int state_st = ISFS_GetFileStats(readFp, read_status);
		if (state_st != ISFS_OK)
		{
			printf("Failed to get file status: %d\n", state_st);
			ret = -2;
		}
		else
		{
			printf("Stats: len: %d pos: %d\n", read_status->file_length, read_status->file_pos);
			usleep(0x2000);
			int read_size = read_status->file_length;
			*buffer = NULL;
			*buffer = memalign(32, read_size * sizeof(char));
			if (*buffer == NULL)
			{
				printf("buffer == NULL\n");
				ret = -3;
			}
			else
			{
				int read = ISFS_Read(readFp, *buffer, read_size);
				if (read != read_size)
				{
					printf("Read %d but should have read %d\n", read, read_size);
					ret = -8;
				}
				*len = read_size;
			}
		}
		free(read_status);
		printf("Doing ISFS_Close()\n");
		int close = ISFS_Close(readFp);
		printf("ISFS_Close() returned %d\n", close);;
	}
	return ret;
}

int WriteNandFile(const char* path, void* buffer, int len)
{
	int ret = 0;

	/* probably don't need these aligned - too lazy to check though */
	u32 ownerId[1] __attribute__((aligned(32)));
	u16 groupId[1] __attribute__((aligned(32)));
	u8  attr[1] __attribute__((aligned(32)));
	u8 ownerPerm[1] __attribute__((aligned(32)));
	u8 groupPerm[1] __attribute__((aligned(32)));
	u8 otherPerm[1] __attribute__((aligned(32)));
	int getAttr = ISFS_GetAttr(path, ownerId, groupId,
			attr, ownerPerm, groupPerm, otherPerm);
	//if (getAttr != ISFS_OK)
	//{
		printf("ISFS_GetAttr(path='%s', ownerId=%#x, groupId=%#x, attr=0x%02x, ownerPerm=0x%02x groupPerm=0x%02x otherPerm=0x%02x): %d\n",
				path, *ownerId, *groupId, *attr, *ownerPerm, *groupPerm, *otherPerm, getAttr);
	//}
	
	if (getAttr == -106)
	{
		/* XXX - path creation */
		int create = ISFS_CreateFile(path, 0, 3, 3, 3);
		if (create < 0)
		{
			printf("ISFS_CreateFile('%s', 0, 3, 3, 3): %d\n", path, create);
			return -4;
		}
	}

	int writeFp = ISFS_Open(path, ISFS_OPEN_RW);
	if (writeFp < 0)
	{
		printf("Failed to open file: %s\n", path);
		ret = -1;
	}
	else
	{
		printf("ISFS_Open() returned %d\n", writeFp);
		int seek = ISFS_Seek(writeFp, 0, SEEK_SET);
		printf("ISFS_Seek() returned %d\n", seek);
		fstats* write_status = static_cast<fstats*>(memalign(32, 8*sizeof(char)));
		int state_st = ISFS_GetFileStats(writeFp, write_status);
		if (state_st != ISFS_OK)
		{
			printf("Failed to get file status: %d\n", state_st);
			ret = -2;
		}
		else
		{
			printf("Stats: len: %d pos: %d\n", write_status->file_length, write_status->file_pos);
			usleep(0x2000);
			if (buffer == NULL)
			{
				printf("buffer == NULL\n");
				ret = -3;
			}
			else
			{
				int write = ISFS_Write(writeFp, buffer, len);
				if (write != len)
				{
					printf("write %d but should have write %d\n", write, len);
					ret = -8;
				}
			}
		}
		free(write_status);
		printf("Doing ISFS_Close()\n");
		int close = ISFS_Close(writeFp);
		printf("ISFS_Close() returned %d\n", close);;
	}
	return ret;
}

int DeleteNandFile(const char* path)
{
	int ret = 0;
	ret = ISFS_Delete(path);
	return ret;
}

//-------------------------------------------------------------
int main(int argc, char **argv) {
//-------------------------------------------------------------

	// Initialise the video system
	VIDEO_Init();
	
	// This function initialises the attached controllers
	WPAD_Init();
	
	// Obtain the preferred video mode from the system
	// This will correspond to the settings in the Wii menu
	rmode = VIDEO_GetPreferredMode(NULL);

	// Allocate memory for the display in the uncached region
	xfb = MEM_K0_TO_K1(SYS_AllocateFramebuffer(rmode));
	
	// Initialise the console, required for printf
	console_init(xfb,20,20,rmode->fbWidth,rmode->xfbHeight,rmode->fbWidth*VI_DISPLAY_PIX_SZ);
	
	// Set up the video registers with the chosen mode
	VIDEO_Configure(rmode);
	
	// Tell the video hardware where our display memory is
	VIDEO_SetNextFramebuffer(xfb);
	
	// Make the display visible
	VIDEO_SetBlack(FALSE);

	// Flush the video register changes to the hardware
	VIDEO_Flush();

	// Wait for Video setup to complete
	VIDEO_WaitVSync();
	if(rmode->viTVMode&VI_NON_INTERLACE) VIDEO_WaitVSync();

	// The console understands VT terminal escape codes
	// This positions the cursor on row 2, column 0
	// we can use variables for this with format codes too
	// e.g. printf ("\x1b[%d;%dH", row, column );
	printf("\x1b[2;0H");
	

	USBGeckoOutput();
	printf("Hello World!\n");

	usleep(0x20000);
	int ret = IOS_Patch();
	printf("IOS_Patch(): %d\n", ret);
	
	/* DO SYSCONF DUMP */
	int isfs_init = ISFS_Initialize();
	if (isfs_init < 0)
	{
		printf("ISFS_Initialize() failed: %d\n", isfs_init);
	}
	else
	{
		printf("ISFS_Initialize() returned %d\n", isfs_init);
		void* buffer;
		int len;
		int read = ReadNandFile("/shared2/sys/SYSCONF", &buffer, &len);
		if (read < 0)
		{
			printf("ReadNandFile('/shared2/sys/SYSCONF', &buffer, &len): %d\n", read);
			u8 dev_info[0x461];
			memset(dev_info, 0, 0x461);
			dev_info[0] = 1;
			u8 bt_dev[6] = { 0xe0, 0xe7, 0x51, 0x9d, 0xec, 0x6e };
			memcpy(dev_info+1, bt_dev, 6);
			strcpy((char*)(dev_info+7), "Nintendo RVL-CNT-01");
			memcpy(dev_info+(43*0x10)+0xd, bt_dev, 6);
			if (!SYSCONF_SetBtDeviceArrayInfo(static_cast<void*>(dev_info)))
				printf("SYSCONF_SetBtDeviceArrayInfo(dev_info) failed\n");
			if (!SYSCONF_SetBtDpdSensibility(3))
				printf("SYSCONF_SetBtDpdSensibility(3) failed\n");
			u8 nik_buf[0x16] = {0, 'n', 0, 'o', 0, 'r', 0, 'm', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 4 };
			if (!SYSCONF_SetOwnerNickName(static_cast<void*>(nik_buf)))
				printf("SYSCONF_SetOwnerNickName(nik_buf) failed\n");
			if (!SYSCONF_SetAspectRatio(SYSCONF_AR_4_3))
				printf("SYSCONF_SetAspectRatio(SYSCONF_AR_4_3) failed\n");
			if (!SYSCONF_SetWpadSensorBarPosition(SYSCONF_BAR_TOP))
				printf("SYSCONF_SetWpadSensorBarPosition(SYSCONF_BAR_TOP) failed\n");
			if (!SYSCONF_SetScreenSaverMode(0))
				printf("SYSCONF_SetScreenSaverMode(0) failed\n");
			if (!SYSCONF_SetLanguage(1))
				printf("SYSCONF_SetLanguage(1) failed\n");
			u8 sadr[0x1008];
			memset(sadr, 0, 0x1008);
			sadr[0] = 0x31;
			if (!SYSCONF_SetSimpleAddressData(static_cast<void*>(sadr)))
				printf("SYSCONF_SetSimpleAddressData(sadr) failed\n");
			if (!SYSCONF_SetCounterBias(0x11c293c3))
				printf("SYSCONF_SetCounterBias(0x11c293c3) failed\n");
			if (!SYSCONF_SetWpadSpeakerVolume(0x58))
				printf("SYSCONF_SetWpadSpeakerVolume(0x58) failed\n");
			u8 parental_control[0x4a];
			memset(parental_control, 0, 0x4a);
			parental_control[1] = 0x01; parental_control[2] = 0x14;
			if (!SYSCONF_SetParentalControl(parental_control))
				printf("SYSCONF_SetParentalControl(parental_control) failed\n");
			if (!SYSCONF_SetNetContentRestrictions(0))
				printf("SYSCONF_SetNetContentRestrictions(0) failed\n");
			if (!SYSCONF_SetWwwRestrictions(0))
				printf("SYSCONF_SetWwwRestrictions(0) failed\n");
			u8 cmp_info[0x205];
			memset(cmp_info, 0, 0x205);
			if (!SYSCONF_SetBtCmpDevInfoArray(static_cast<void*>(cmp_info)))
				printf("SYSCONF_SetBtCmpDevInfoArray(cmp_info) failed\n");
			if (!SYSCONF_SetInstalledChannelAppCount(0xc))
				printf("SYSCONF_SetInstalledChannelAppCount(0xc) failed\n");
			if (!SYSCONF_SetFreeChannelAppCount(0x24))
				printf("SYSCONF_SetFreeChannelAppCount(0x24) failed\n");
			if (!SYSCONF_SetConfigDoneFlag(true))
				printf("SYSCONF_SetConfigDoneFlag(true) failed\n");
			if (!SYSCONF_SetConfigDoneFlag2(true))
				printf("SYSCONF_SetConfigDoneFlag2(true) failed\n");
			if (!SYSCONF_SetUpdateType(1))
				printf("SYSCONF_SetUpdateType(1) failed\n");
			if (!SYSCONF_SetWpadMotorMode(1))
				printf("SYSCONF_SetWpadMotorMode(1) failed\n");
			if (!SYSCONF_SetEula(true))
				printf("SYSCONF_SetEula(true) failed\n");
			if (!SYSCONF_SetWCFlags(1))
				printf("SYSCONF_SetWCFlags(1) failed\n");
			if (!SYSCONF_SetIdleMode(true, true))
				printf("SYSCONF_SetIdleMode(true, true) failed\n");
			void* written = WriteSysconf();
			WriteNandFile("/shared2/sys/SYSCONF", written, 0x4000);
			free(written);
		}
		else
		{
			printf("ReadNandFile('/shared2/sys/SYSCONF', &buffer, &len): %d\n", read);
#if 0
			DebugSysconf(buffer);
			ghexdump(buffer, 0x4000);
#else
			int check = CheckSysconf(buffer);
			printf("CheckSysconf(buffer): %d\n", check);
			if (check >= 0)
			{
				ReadSysconf(buffer);
				void* written = WriteSysconf();
				printf("\n\nHow'd we do?: %d\n\n", memcmp(buffer, written, 0x4000));
				ghexdump(buffer, 0x4000);
				printf("\n\n");
				ghexdump(written, 0x4000);
				DeleteNandFile("/shared2/sys/SYSCONF");
				WriteNandFile("/shared2/sys/SYSCONF", written, 0x4000);
				free(written);
			}
#endif
			free(buffer);
		}
	}

	return 0;
}

