#ifndef __GECKO_H
#define __GECKO_H

#ifdef __cplusplus
extern "C" {
#endif

void USBGeckoOutput(void);
void ghexdump(void *d, int len);

#ifdef __cplusplus
}
#endif

#endif
