#include <gccore.h>
#include <stdio.h>
#include "gecko.h"

#include <sys/iosupport.h>

/****************************************************************************
 *  USB Gecko Debugging
 ***************************************************************************/

static bool gecko = false;
static mutex_t gecko_mutex = 0;

static ssize_t __out_write(struct _reent *r, int fd, const char *ptr, size_t len)
{
	if (!gecko || len == 0)
		return len;

	if(!ptr || len < 0)
		return -1;

	u32 level;
	LWP_MutexLock(gecko_mutex);
	level = IRQ_Disable();
	//usb_sendbuffer(1, ptr, len);
	usb_sendbuffer_safe(1, ptr, len);
	IRQ_Restore(level);
	LWP_MutexUnlock(gecko_mutex);
	return len;
}

const devoptab_t gecko_out = {
	"stdout",	// device name
	0,			// size of file structure
	NULL,		// device open
	NULL,		// device close
	__out_write,// device write
	NULL,		// device read
	NULL,		// device seek
	NULL,		// device fstat
	NULL,		// device stat
	NULL,		// device link
	NULL,		// device unlink
	NULL,		// device chdir
	NULL,		// device rename
	NULL,		// device mkdir
	0,			// dirStateSize
	NULL,		// device diropen_r
	NULL,		// device dirreset_r
	NULL,		// device dirnext_r
	NULL,		// device dirclose_r
	NULL		// device statvfs_r
};

void USBGeckoOutput()
{
	gecko = usb_isgeckoalive(1);
	LWP_MutexInit(&gecko_mutex, false);

	devoptab_list[STD_OUT] = &gecko_out;
	devoptab_list[STD_ERR] = &gecko_out;
}

char ascii(char s)
{
	if(s < 0x20)
		return '.';
	if(s > 0x7E)
		return '.';
	return s;
}

void ghexdump(void *d, int len)
{
	u8 *data;
	int i, off;
	data = (u8*)d;

	//printf("\n       0  1  2  3  4  5  6  7  8  9  A  B  C  D  E  F  0123456789ABCDEF");
	printf(  "          0  1  2  3  4  5  6  7  8  9  A  B  C  D  E  F   0123456789ABCDEF");
	printf("\n========  ===============================================  ================\n");

	for (off = 0; off < len; off += 16)
	{
		printf("%04x  ",off);
		//printf("%08x  ",(u32)(data + off));
		for(i = 0; i < 16; i++)
		{
			if((i+off)>=len)
				printf("   ");
			else
				printf("%02x ",data[off+i]);
		}
		printf(" ");
		for(i = 0; i < 16; i++)
		{
			if((i+off)>=len)
				printf(" ");
			else
				printf("%c",ascii(data[off+i]));
		}
		printf("\n");
	}
}

