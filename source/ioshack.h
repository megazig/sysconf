#ifndef _IOSHACK_H_
#define _IOSHACK_H_

#include <gccore.h>

#define HW_AHBPROT		0x0d800064
#define MEM_PROT		0x0d8b420a

#ifdef __cplusplus
extern "C" {
#endif

int IOS_Patch(void);

#ifdef __cplusplus
}
#endif

#endif
